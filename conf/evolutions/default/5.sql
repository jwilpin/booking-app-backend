# --- DDL to schedule domain

# --- !Ups

# --- Routes from http://openflights.org/data.html
CREATE TABLE `routes` (
  `airline` varchar(3) DEFAULT NULL,
  `alid` int(11) DEFAULT NULL,
  `src_ap` varchar(4) DEFAULT NULL,
  `src_apid` int(11) DEFAULT NULL,
  `dst_ap` varchar(4) DEFAULT NULL,
  `dst_apid` int(11) DEFAULT NULL,
  `codeshare` text,
  `stops` text,
  `equipment` text,
  `added` varchar(1) DEFAULT NULL,
  `rid` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`rid`),
  UNIQUE KEY `alid` (`alid`,`src_apid`,`dst_apid`),
  KEY `src_apid` (`src_apid`),
  KEY `dst_apid` (`dst_apid`)
);

# --- Inserts/Load data defined in 6 to 10 evolutions

# --- !Downs
drop table `routes`;
