import play.PlayScala

name := """booking-app-backend"""

version := "1.0"

resolvers += "Atlassian" at "https://maven.atlassian.com/content/repositories/atlassian-public/"

libraryDependencies += filters

libraryDependencies ++= Seq(
  "com.mohiva" %% "play-silhouette" % "2.0",
  "org.webjars" %% "webjars-play" % "2.3.0",
  "org.webjars" % "bootstrap" % "3.1.1",
  "org.webjars" % "jquery" % "1.11.0",
  "net.codingwell" %% "scala-guice" % "4.0.0-beta4",
  "com.typesafe.play" %% "play-slick" % "0.8.1",
  "mysql" % "mysql-connector-java" % "5.1.32",
  "com.typesafe.slick" %% "slick-codegen" % "2.1.0",
  "com.typesafe.slick" %% "slick" % "2.1.0",
  "joda-time" % "joda-time" % "2.4",
  "org.joda" % "joda-convert" % "1.6",
  "com.github.tototoshi" %% "slick-joda-mapper" % "1.2.0",
  cache
)

lazy val root = (project in file(".")).enablePlugins(PlayScala)
