package security.controllers

import java.util.UUID
import javax.inject.Inject

import com.mohiva.play.silhouette.api._
import com.mohiva.play.silhouette.api.services.{AuthInfoService, AvatarService}
import com.mohiva.play.silhouette.api.util.PasswordHasher
import com.mohiva.play.silhouette.impl.authenticators.JWTAuthenticator
import com.mohiva.play.silhouette.impl.providers.{CommonSocialProfile, CredentialsProvider}
import user.services.{UserCommandsService, UserQueriesService}
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.json.{JsError, Json}
import play.api.mvc.Action
import security.json.SignUp
import user.model.User
import sharedkernel.common.json.Format._
import scala.concurrent.Future

/**
 * The sign up controller.
 *
 * @param env The Silhouette environment.
 * @param userCommandsService The user service implementation.
 * @param authInfoService The auth info service implementation.
 * @param avatarService The avatar service implementation.
 * @param passwordHasher The password hasher implementation.
 */
class SignUpController @Inject() (
                                   implicit val env: Environment[User, JWTAuthenticator],
                                   val userCommandsService: UserCommandsService,
                                   val userQueriesService: UserQueriesService,
                                   val authInfoService: AuthInfoService,
                                   val avatarService: AvatarService,
                                   val passwordHasher: PasswordHasher)
  extends Silhouette[User, JWTAuthenticator] {

  import security.json.Format._

  /**
   * Registers a new user.
   *
   * @return The result to display.
   */
  def signUp = Action.async(parse.json[SignUp]) { implicit request =>
    val signup = request.body
    val loginInfo = LoginInfo(CredentialsProvider.ID, signup.email)
    val authInfo = passwordHasher.hash(signup.password)
    val socialProfile = CommonSocialProfile(loginInfo, signup.firstName, signup.lastName, Some(signup.firstName.getOrElse("") + " " + signup.lastName.getOrElse("")), Some(signup.email), None)
    userQueriesService.retrieve(loginInfo).flatMap {
      case Some(user) =>
        Future.successful(BadRequest(Json.toJson("user with same email already exists")))
      case None =>
        val user = User(
          userID = UUID.randomUUID(),
          loginInfo = loginInfo,
          firstName = signup.firstName,
          lastName = signup.lastName,
          fullName = Some(signup.firstName.getOrElse("") + " " + signup.lastName.getOrElse("")),
          email = Some(signup.email),
          avatarURL = None
        )
        for {
          avatar <- avatarService.retrieveURL(signup.email)
          userToSave <- userCommandsService.save(socialProfile.copy(avatarURL = avatar))
          user <- userCommandsService.save(userToSave)
          authInfo <- authInfoService.save(loginInfo, authInfo)
          authenticator <- env.authenticatorService.create(loginInfo)
          token <- env.authenticatorService.init(authenticator)
          result <- env.authenticatorService.embed(token, Future.successful {
            Ok(Json.toJson(Json.obj("token" -> token)))
          })
        } yield {
          env.eventBus.publish(SignUpEvent(user, request, request2lang))
          env.eventBus.publish(LoginEvent(user, request, request2lang))
          result
        }
    }.recoverWith(exceptionHandler)
  }
}
