package security.json

case class SignUp(
   password: String,
   email: String,
   firstName: Option[String],
   lastName: Option[String],
   fullName: Option[String]
)

case class SignIn(email: String, password: String, rememberMe: Boolean)
