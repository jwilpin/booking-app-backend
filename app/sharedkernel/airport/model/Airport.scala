package sharedkernel.airport.model

case class Airport(
  apid: Int,
  name: String,
  iata: Option[String],
  icao: Option[String],
  city: Option[String],
  country: Option[String]
)

object Airport {

  val default = Airport(
    -1,
    "Default",
    None,
    None,
    None,
    None
  )

}
