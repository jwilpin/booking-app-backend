package sharedkernel.airport

import javax.inject.Inject

import com.mohiva.play.silhouette.api.{Environment, Silhouette}
import com.mohiva.play.silhouette.impl.authenticators.JWTAuthenticator
import play.api.libs.json.Json
import sharedkernel.airport.services.AirportQueriesService
import user.model.User
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import sharedkernel.airport.json.Format._
import scala.concurrent.Future

class AirportController @Inject()(
                                   implicit val env: Environment[User, JWTAuthenticator],
                                   airportQueriesService: AirportQueriesService)
  extends Silhouette[User, JWTAuthenticator] {

  def find(airportQuery: String) = UserAwareAction.async { implicit request =>
    if(airportQuery.trim.length < 3) Future.successful(BadRequest)
    else airportQueriesService.list(Some(airportQuery)).map(airports => Ok(Json.toJson(airports)))
  }

  def list() = UserAwareAction.async { implicit request =>
    airportQueriesService.list(None).map(airports => Ok(Json.toJson(airports)))
  }

}
