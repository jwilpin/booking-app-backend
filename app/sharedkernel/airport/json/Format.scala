package sharedkernel.airport.json

import sharedkernel.airport.model.Airport
import play.api.libs.json._

object Format {
    implicit val airportWrites = Json.writes[Airport]
    implicit val airportReads = Json.reads[Airport]
}
