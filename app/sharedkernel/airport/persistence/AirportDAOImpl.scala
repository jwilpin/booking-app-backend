package sharedkernel.airport.persistence

import play.api.db.slick.Config.driver.simple._
import play.api.db.slick._
import sharedkernel.common.persistence.Tables.{AirportsTable}
import sharedkernel.airport.model.Airport

/**
 * Give access to the user object using Slick
 */
class AirportDAOImpl extends AirportDAO {

  import play.api.Play.current

  def list(airportQuery: Option[String]): List[Airport] =
    DB withSession { implicit session =>
      val data = airportQuery match {
        case Some(query) => AirportsTable.filter(row =>
            row.name.toLowerCase.startsWith(airportQuery.get.toLowerCase) ||
            row.city.getOrElse("").toLowerCase.startsWith(airportQuery.get.toLowerCase) ||
            row.country.getOrElse("").toLowerCase.startsWith(airportQuery.get.toLowerCase)
        ).run
        case None => AirportsTable.run
      }
      data.map(row =>
          Airport(
            row.apid,
            row.name,
            row.iata,
            row.icao,
            row.city,
            row.country
          )
        ).toList
    }

  def find(aid: Int): Option[Airport] =
    DB withSession { implicit session =>
      AirportsTable
        .filter(row => row.apid === aid)
        .mapResult(row =>
          Airport(
            row.apid,
            row.name,
            row.iata,
            row.icao,
            row.city,
            row.country
          )
        ).firstOption
    }

}
