package sharedkernel.airport.services

import javax.inject.Inject

import sharedkernel.airport.model.Airport
import sharedkernel.airport.persistence.AirportDAO

import play.api.libs.concurrent.Execution.Implicits.defaultContext
import scala.concurrent.Future

/**
 * Handles queries actions to airport.
 *
 * @param airportDAO The airport DAO implementation.
 */
class AirportQueriesServiceImpl @Inject()(airportDAO: AirportDAO) extends AirportQueriesService {

  def list(airportQuery: Option[String]): Future[List[Airport]] = Future.successful(airportDAO.list(airportQuery))

}
