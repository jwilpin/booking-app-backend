package sharedkernel.airport.services

import sharedkernel.airport.model.Airport

import scala.concurrent.Future

trait AirportQueriesService {

  def list(airportQuery: Option[String]): Future[List[Airport]]

}
