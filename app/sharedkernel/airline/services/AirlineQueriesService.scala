package sharedkernel.airline.services

import sharedkernel.airline.model.Airline

import scala.concurrent.Future

trait AirlineQueriesService {

  def find(aid: Int): Future[Option[Airline]]

}
