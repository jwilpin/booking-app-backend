package sharedkernel.airline.json

import sharedkernel.airline.model.Airline
import play.api.libs.json._

object Format {
    implicit val airlineWrites = Json.writes[Airline]
    implicit val airlineReads = Json.reads[Airline]
}
