package sharedkernel.common.persistence

import scala.slick.driver.JdbcProfile
import scala.slick.model.Model
import slick.codegen.SourceCodeGenerator

class TablesGenerator(model: Model) extends SourceCodeGenerator(model) {

  override def code = s"""
    import sharedkernel.common.persistence.TypeMappers._
    import com.github.tototoshi.slick.MySQLJodaSupport._
    import org.joda.time.DateTime
    """ + super.code

  def defTypeCode(table: Table) = table.EntityType.docWithCode

  override def entityName = dbTableName => {
    val name = dbTableName match {
      case _ => dbTableName.toCamelCase
    }
    s"${name}Row"
  }

  override def tableName = dbTableName => {
    val name = dbTableName match {
      case _ => dbTableName.toCamelCase.capitalize
    }
    s"${name}Table"
  }

  override def Table = new Table(_) {
    table =>
      override def Column = new Column(_) {
        // customize Scala column names
        override def rawName = (table.model.name.table, this.model.name) match {
          case _ => super.rawName;
        }
        override def rawType = model.tpe match {
          case "java.sql.Timestamp" => "DateTime" // kill j.s.Timestamp
          case "scala.math.BigDecimal"  => "Float"
          case _ =>
            (model.name, model.table.table) match  {
              case ("status","schedules") =>
                "schedule.model.ScheduleStatus.Value"
              case _ =>  super.rawType
            }
        }
      }
  }

}

object TablesGenerator extends App {

  // http://slick.typesafe.com/doc/2.1.0/code-generation.html
  val slickDriver = "scala.slick.driver.MySQLDriver"
  val jdbcDriver = "com.mysql.jdbc.Driver"
  val url = "jdbc:mysql://localhost/booking-app?characterEncoding=UTF-8"
  val outputFolder = "app"
  val pkg = "sharedkernel.common.persistence"
  val user = "user"
  val password = "changeit"

  val driver: JdbcProfile = scala.slick.driver.MySQLDriver

  val db = {
    driver.simple.Database.forURL(url, driver = jdbcDriver, user = user, password = password)
  }

  db.withSession { implicit session =>
    val tables = Some(driver.defaultTables.filter(t => !t.name.name.equals("play_evolutions")))
    new TablesGenerator(driver.createModel(tables)).writeToFile(slickDriver, outputFolder, pkg)
  }

}
