import com.google.inject.{AbstractModule, Provides}
import com.mohiva.play.silhouette.api.{Environment, EventBus}
import com.mohiva.play.silhouette.api.services.{AuthInfoService, AuthenticatorService, AvatarService}
import com.mohiva.play.silhouette.api.util._
import com.mohiva.play.silhouette.impl.authenticators.{JWTAuthenticator, JWTAuthenticatorService, JWTAuthenticatorSettings}
import com.mohiva.play.silhouette.impl.daos.DelegableAuthInfoDAO
import com.mohiva.play.silhouette.impl.providers._
import com.mohiva.play.silhouette.impl.providers.oauth1.TwitterProvider
import com.mohiva.play.silhouette.impl.providers.oauth1.secrets.{CookieSecretProvider, CookieSecretSettings}
import com.mohiva.play.silhouette.impl.providers.oauth1.services.PlayOAuth1Service
import com.mohiva.play.silhouette.impl.providers.oauth2.state.DummyStateProvider
import com.mohiva.play.silhouette.impl.providers.oauth2.{FacebookProvider, GoogleProvider}
import com.mohiva.play.silhouette.impl.services.{DelegableAuthInfoService, GravatarService}
import com.mohiva.play.silhouette.impl.util.{BCryptPasswordHasher, PlayCacheLayer, SecureRandomIDGenerator}
import net.codingwell.scalaguice.ScalaModule
import play.Logger
import play.api.Play
import play.api.Play.current
import schedule.persistence.{ScheduleDAO, ScheduleDAOImpl}
import schedule.route.persistence.{RouteDAO, RouteDAOImpl}
import schedule.route.services.{RouteQueriesService, RouteQueriesServiceImpl}
import schedule.services.{ScheduleCommandsService, ScheduleCommandsServiceImpl, ScheduleQueriesService, ScheduleQueriesServiceImpl}
import security.persistence._
import sharedkernel.airline.persistence.{AirlineDAO, AirlineDAOImpl}
import sharedkernel.airline.services.{AirlineQueriesService, AirlineQueriesServiceImpl}
import sharedkernel.airport.persistence.{AirportDAO, AirportDAOImpl}
import sharedkernel.airport.services.{AirportQueriesService, AirportQueriesServiceImpl}
import user.model.User
import user.persistence.{UserDAO, UserDAOImpl}
import user.services.{UserCommandsService, UserCommandsServiceImpl, UserQueriesService, UserQueriesServiceImpl}

/**
 * The Guice module which wires all Silhouette dependencies.
 */
class Injector extends AbstractModule with ScalaModule {

  /**
   * Configures the module.
   */
  def configure() {
    Logger.debug("Binding to shared kernel implementations.")
    bind[AirportDAO].to[AirportDAOImpl]
    bind[AirportQueriesService].to[AirportQueriesServiceImpl]
    bind[AirlineDAO].to[AirlineDAOImpl]
    bind[AirlineQueriesService].to[AirlineQueriesServiceImpl]

    Logger.debug("Binding to schedule implementations.")
    bind[RouteDAO].to[RouteDAOImpl]
    bind[RouteQueriesService].to[RouteQueriesServiceImpl]
    bind[ScheduleDAO].to[ScheduleDAOImpl]
    bind[ScheduleQueriesService].to[ScheduleQueriesServiceImpl]
    bind[ScheduleCommandsService].to[ScheduleCommandsServiceImpl]

    Logger.debug("Binding to user implementations.")
    bind[UserCommandsService].to[UserCommandsServiceImpl]
    bind[UserQueriesService].to[UserQueriesServiceImpl]
    bind[UserDAO].to[UserDAOImpl]

    Logger.debug("Binding to security implementations.")
    bind[DelegableAuthInfoDAO[PasswordInfo]].to[PasswordInfoDAOImpl]
    bind[DelegableAuthInfoDAO[OAuth1Info]].to[OAuth1InfoDAOImpl]
    bind[DelegableAuthInfoDAO[OAuth2Info]].to[OAuth2InfoDAOImpl]
    bind[CacheLayer].to[PlayCacheLayer]
    bind[HTTPLayer].to[PlayHTTPLayer]
    bind[IDGenerator].toInstance(new SecureRandomIDGenerator())
    bind[PasswordHasher].toInstance(new BCryptPasswordHasher)
    bind[EventBus].toInstance(EventBus())
  }

  /**
   * Provides the Silhouette environment.
   *
   * @param userQueriesService The user queries service implementation.
   * @param authenticatorService The authentication service implementation.
   * @param eventBus The event bus instance.
   * @return The Silhouette environment.
   */
  @Provides
  def provideEnvironment(
                          userQueriesService: UserQueriesService,
                          authenticatorService: AuthenticatorService[JWTAuthenticator],
                          eventBus: EventBus,
                          credentialsProvider: CredentialsProvider,
                          facebookProvider: FacebookProvider,
                          googleProvider: GoogleProvider,
                          twitterProvider: TwitterProvider): Environment[User, JWTAuthenticator] = {

    Environment[User, JWTAuthenticator](
      userQueriesService,
      authenticatorService,
      Map(
        credentialsProvider.id -> credentialsProvider,
        facebookProvider.id -> facebookProvider,
        googleProvider.id -> googleProvider,
        twitterProvider.id -> twitterProvider
      ),
      eventBus
    )
  }

  /**
   * Provides the authenticator service.
   *
   * @param cacheLayer The cache layer implementation.
   * @param idGenerator The ID generator used to create the authenticator ID.
   * @return The authenticator service.
   */
  @Provides
  def provideAuthenticatorService(
    cacheLayer: CacheLayer,
    idGenerator: IDGenerator): AuthenticatorService[JWTAuthenticator] = {
    val settings = JWTAuthenticatorSettings(
      headerName = Play.configuration.getString("silhouette.authenticator.headerName").getOrElse { "X-Auth-Token" },
      issuerClaim = Play.configuration.getString("silhouette.authenticator.issueClaim").getOrElse { "play-silhouette" },
      encryptSubject = Play.configuration.getBoolean("silhouette.authenticator.encryptSubject").getOrElse { true },
      authenticatorIdleTimeout = Play.configuration.getInt("silhouette.authenticator.authenticatorIdleTimeout"), // This feature is disabled by default to prevent the generation of a new JWT on every request
      authenticatorExpiry = Play.configuration.getInt("silhouette.authenticator.authenticatorExpiry").getOrElse { 12 * 60 * 60 },
      sharedSecret = Play.configuration.getString("application.secret").get)
    new JWTAuthenticatorService(
      settings = settings,
      dao = None,
      idGenerator = idGenerator,
      clock = Clock())
  }

  /**
   * Provides the auth info service.
   *
   * @param passwordInfoDAO The implementation of the delegable password auth info DAO.
   * @param oauth1InfoDAO The implementation of the delegable OAuth1 auth info DAO.
   * @param oauth2InfoDAO The implementation of the delegable OAuth2 auth info DAO.
   * @return The auth info service instance.
   */
  @Provides
  def provideAuthInfoService(
    passwordInfoDAO: DelegableAuthInfoDAO[PasswordInfo],
    oauth1InfoDAO: DelegableAuthInfoDAO[OAuth1Info],
    oauth2InfoDAO: DelegableAuthInfoDAO[OAuth2Info]): AuthInfoService = {

    new DelegableAuthInfoService(passwordInfoDAO, oauth1InfoDAO, oauth2InfoDAO)
  }

  /**
   * Provides the avatar service.
   *
   * @param httpLayer The HTTP layer implementation.
   * @return The avatar service implementation.
   */
  @Provides
  def provideAvatarService(httpLayer: HTTPLayer): AvatarService = new GravatarService(httpLayer)

  /**
   * Provides the credentials provider.
   *
   * @param authInfoService The auth info service implemenetation.
   * @param passwordHasher The default password hasher implementation.
   * @return The credentials provider.
   */
  @Provides
  def provideCredentialsProvider(
    authInfoService: AuthInfoService,
    passwordHasher: PasswordHasher): CredentialsProvider = {

    new CredentialsProvider(authInfoService, passwordHasher, Seq(passwordHasher))
  }

  /**
   * Provides the Facebook provider.
   *
   * @param cacheLayer The cache layer implementation.
   * @param httpLayer The HTTP layer implementation.
   * @return The Facebook provider.
   */
  @Provides
  def provideFacebookProvider(cacheLayer: CacheLayer, httpLayer: HTTPLayer): FacebookProvider = {
    FacebookProvider(httpLayer, new DummyStateProvider, OAuth2Settings(
      authorizationURL = Play.configuration.getString("silhouette.facebook.authorizationURL"),
      accessTokenURL = Play.configuration.getString("silhouette.facebook.accessTokenURL").get,
      redirectURL = Play.configuration.getString("silhouette.facebook.redirectURL").get,
      clientID = Play.configuration.getString("silhouette.facebook.clientID").get,
      clientSecret = Play.configuration.getString("silhouette.facebook.clientSecret").get,
      scope = Play.configuration.getString("silhouette.facebook.scope")))
  }

  /**
   * Provides the Google provider.
   *
   * @param cacheLayer The cache layer implementation.
   * @param httpLayer The HTTP layer implementation.
   * @return The Google provider.
   */
  @Provides
  def provideGoogleProvider(cacheLayer: CacheLayer, httpLayer: HTTPLayer): GoogleProvider = {
    GoogleProvider(httpLayer, new DummyStateProvider, OAuth2Settings(
      authorizationURL = Play.configuration.getString("silhouette.google.authorizationURL"),
      accessTokenURL = Play.configuration.getString("silhouette.google.accessTokenURL").get,
      redirectURL = Play.configuration.getString("silhouette.google.redirectURL").get,
      clientID = Play.configuration.getString("silhouette.google.clientID").get,
      clientSecret = Play.configuration.getString("silhouette.google.clientSecret").get,
      scope = Play.configuration.getString("silhouette.google.scope")))
  }

  @Provides
  def provideTwitterProvider(cacheLayer: CacheLayer, httpLayer: HTTPLayer): TwitterProvider = {
    val oAuth1TokenSecretProvider: OAuth1TokenSecretProvider = {
      new CookieSecretProvider(CookieSecretSettings(
        cookieName = Play.configuration.getString("silhouette.oauth1TokenSecretProvider.cookieName").get,
        cookiePath = Play.configuration.getString("silhouette.oauth1TokenSecretProvider.cookiePath").get,
        cookieDomain = Play.configuration.getString("silhouette.oauth1TokenSecretProvider.cookieDomain"),
        secureCookie = Play.configuration.getBoolean("silhouette.oauth1TokenSecretProvider.secureCookie").get,
        httpOnlyCookie = Play.configuration.getBoolean("silhouette.oauth1TokenSecretProvider.httpOnlyCookie").get,
        expirationTime = Play.configuration.getInt("silhouette.oauth1TokenSecretProvider.expirationTime").get
      ), Clock())
    }
    val settings = OAuth1Settings(
      requestTokenURL = Play.configuration.getString("silhouette.twitter.requestTokenURL").get,
      accessTokenURL = Play.configuration.getString("silhouette.twitter.accessTokenURL").get,
      authorizationURL = Play.configuration.getString("silhouette.twitter.authorizationURL").get,
      callbackURL = Play.configuration.getString("silhouette.twitter.callbackURL").get,
      consumerKey = Play.configuration.getString("silhouette.twitter.consumerKey").get,
      consumerSecret = Play.configuration.getString("silhouette.twitter.consumerSecret").get)

    TwitterProvider(httpLayer, new PlayOAuth1Service(settings), oAuth1TokenSecretProvider, settings)
  }
}


object Injector {
  val injector = com.google.inject.Guice.createInjector(new Injector())
}