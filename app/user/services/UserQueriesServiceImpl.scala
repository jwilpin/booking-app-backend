package user.services

import javax.inject.Inject

import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.impl.providers.CommonSocialProfile
import user.model.User
import user.persistence.UserDAO

import scala.concurrent.Future

/**
 * Handles queries actions to users.
 *
 * @param userDAO The user DAO implementation.
 */
class UserQueriesServiceImpl @Inject()(userDAO: UserDAO) extends UserQueriesService {

  /**
   * Retrieves a user that matches the specified login info.
   *
   * @param loginInfo The login info to retrieve a user.
   * @return The retrieved user or None if no user could be retrieved for the given login info.
   */
  def retrieve(loginInfo: LoginInfo): Future[Option[User]] = userDAO.find(loginInfo)

}
