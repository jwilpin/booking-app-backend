package user.json

import play.api.libs.json.Json
import user.model.User

object Format {
  implicit val userWrites = Json.writes[User]
  implicit val userReads = Json.reads[User]
}
