package schedule.persistence

import schedule.json.ScheduleBasicQuery
import schedule.model.{Schedule, ScheduleStatus}

trait ScheduleDAO {

  def retrieve(basicQuery: ScheduleBasicQuery): List[Schedule]

  def updateScheduleSeatsAndStatus(scheduleId: Int, bookedSeats: Int, status: ScheduleStatus.Value): Int

  /** Used to create mock schedules */

  def list: List[Schedule]

  def create(schedule: Schedule): Schedule

}
