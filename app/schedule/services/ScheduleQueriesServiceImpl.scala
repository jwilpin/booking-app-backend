package schedule.services

import javax.inject.Inject

import schedule.json.ScheduleBasicQuery
import schedule.model.Schedule
import schedule.persistence.ScheduleDAO

import play.api.libs.concurrent.Execution.Implicits.defaultContext
import scala.concurrent.Future

class ScheduleQueriesServiceImpl @Inject()(scheduleDAO: ScheduleDAO) extends ScheduleQueriesService {

  def retrieve(basicQuery: ScheduleBasicQuery): Future[List[Schedule]] =
    Future.successful(scheduleDAO.retrieve(basicQuery))

  /** Used to create mock schedules */

  def list: Future[List[Schedule]] =
    Future.successful(scheduleDAO.list)

}
