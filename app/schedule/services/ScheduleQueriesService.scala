package schedule.services

import schedule.json.ScheduleBasicQuery
import schedule.model.Schedule

import scala.concurrent.Future

trait ScheduleQueriesService {

  def retrieve(basicQuery: ScheduleBasicQuery): Future[List[Schedule]]

  /** Used to create mock schedules */

  def list: Future[List[Schedule]]
}
