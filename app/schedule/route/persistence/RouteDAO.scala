package schedule.route.persistence

import schedule.route.model.Route
import sharedkernel.airport.model.Airport

trait RouteDAO {

  def find(rid: Int): Option[Route]

  def find(src: Airport, dst: Airport): List[Route]

  def list(): List[Route]

}
