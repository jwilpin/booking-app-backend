package schedule.route.services

import schedule.route.model.Route
import sharedkernel.airport.model.Airport

import scala.concurrent.Future

trait RouteQueriesService {

  def find(rid: Int): Future[Option[Route]]

  def find(src: Airport, dst: Airport): Future[List[Route]]

  def list(): Future[List[Route]]

}
