package schedule

import javax.inject.Inject

import com.mohiva.play.silhouette.api.{Environment, Silhouette}
import com.mohiva.play.silhouette.impl.authenticators.JWTAuthenticator
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Json
import schedule.json.ScheduleBasicQuery
import schedule.services.ScheduleQueriesService
import user.model.User
import schedule.json.Format._

import scala.concurrent.Future

class ScheduleController @Inject()(
                                   implicit val env: Environment[User, JWTAuthenticator],
                                   scheduleQueriesService: ScheduleQueriesService)
  extends Silhouette[User, JWTAuthenticator] {

  def findSchedules() = UserAwareAction.async (parse.json[ScheduleBasicQuery]) { implicit request =>
    val query = request.body
    scheduleQueriesService.retrieve(query).map(schedule => Ok(Json.toJson(schedule)))
  }

}
